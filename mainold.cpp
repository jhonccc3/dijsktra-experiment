#include <utility>
#include <algorithm>
#include <iostream>
#include <cstdlib>
#include <string>
#include <vector>


#include "utils/utils.h"

#include "models/Field.h"
#include "models/Node.h"
#include "models/Product.h"
#include "models/Customer.h"


#include "controller/BaseController.h"
#include "models/Database.h"
#include "models/DBMergeSort.h"
#include "models/DBMergeBSort.h"

#include "models/BTree.h"
#include "models/BTreeLeaf.h"
#include "models/BTreeNode.h"


#include "controller/AbstractController.h"
#include "controller/CustomerController.h"
#include "controller/ProductController.h"
#include "controller/QueryDatabase.h"


class A {
    public:
        int var= 9993;
        int a;
        virtual int show(A* ac){
            a = 1;
            return 3;
        }

        virtual int ame(){
            return var;
        }

        static A* create(){
            return new A;
        }
};
class B : public A {
    
    public:
        int var = 888;
        int show (A* ac){
            a = 2;
            return ac->a;
        }

        int show2 (A* ac){
            a = 2;
            return A::ame();
        }

        static B* create(){
            return new B;
        }
};


void print(vector<ifstream*> fs){
    std::string s;
    std::getline(*fs[0], s);
    std::cout<< s << endl;
}

// test clases
int mai2n() {
    B* b = B::create();
    A* a = b;
    B* c = dynamic_cast<B*>(a);
    std::cout << c->show(a) << endl;
    std::cout << c->show2(a) << endl;

    std::istringstream iss("1,asdf,ds");
    std::string segment; 
    std::getline(iss, segment, ',');
    int num = std::stoi(segment, nullptr);
    std:: cout << num << endl;

    std::vector <A*> as;
    
    as.push_back(new A());
    as.push_back(new A());
    A** add = as.data();
    std::cout << add[0]->show(new A()) << endl;
    std::cout << add[1]->show(new A()) << endl;
    A* aaa = as.back();
    std::cout << as.size() << endl;
    A* addfa[2];
    addfa[0] = as.back();
    addfa[1] = as.back();
    std::cout << addfa[1] << endl;

    std::vector<ifstream*> fs;
    std::ifstream f;
    std::string s = "tmp/output.sorted";
    f.open(s.c_str(), ios::in);
    fs.push_back(&f);
    std::getline(*fs[0], s);
    std::cout<< s << endl;
    print(fs);
}

int handlecommands(int argc, char *argv[]) {
    // contants
    std::string datafolder = BaseController::dataFolder();

    // define console commands
    std::cout << "INITIALIZING THE EXTERNAL MEMORY EXPERIMENT" << std::endl;
    if (console::exist(argv, argv + argc, "--model") && console::exist(argv, argv + argc, "--samples")) {

        std::string model = console::get(argv, argv + argc, "--model");
        int samples = std::atoi(console::get(argv, argv + argc, "--samples"));

        if(model == "Customer") CustomerController::generateRandomSamples(samples);
        else if(model == "Product") ProductController::generateRandomSamples(samples);
        else std::cout << "---> Invalid Model Name" << std::endl;

    } else if (console::exist(argv, argv + argc, "--model") && console::exist(argv, argv + argc, "--sortByField")) { //
        std::string model = console::get(argv, argv + argc, "--model");
        std::string fieldName = console::get(argv, argv + argc, "--sortByField");

        if(model == "Customer") (new CustomerController)->sortByField(fieldName);
        else if(model == "Product") (new ProductController)->sortByField(fieldName);
        else std::cout << "---> Invalid Model Name" << std::endl;

    } else if (console::exist(argv, argv + argc, "--generate-data")) { // generate data for tables
        std::string model = console::get(argv, argv + argc, "--generate-data");

        BaseController::cleanDataFolder();
        if (model == "Customer")
            for(int i = 1; i < 4; i ++)
                (new CustomerController())->generateData(pow(10, i), datafolder + model + std::to_string(i) + ".db");
        else if (model == "Product") 
            for(int i = 1; i < 4; i ++)
                (new ProductController())->generateData(pow(10, i), datafolder + model + std::to_string(i) + ".db");
        else std::cout << "---> Invalid Model Name" << std::endl;
        
        
    } else if (console::exist(argv, argv + argc, "--run-sorting")) { // run experiment with data listed in 
        // ./a.out --run-sorting Product --field price --dbsize 7 --fsize 1 --times 1  --meth MSB --o exp2out
        std::string model = console::get(argv, argv + argc, "--run-sorting");
        std::string dbsize = console::get(argv, argv + argc, "--dbsize");
        std::string fsize = console::get(argv, argv + argc, "--fsize");
        std::string times = console::get(argv, argv + argc, "--times");
        std::string field = console::get(argv, argv + argc, "--field");
        std::string meth = console::get(argv, argv + argc, "--meth");
        std::string output = console::get(argv, argv + argc, "--o");

        if(model == "Customer") 
            (new CustomerController())->runSortingExperimentWith(meth, dbsize, fsize, times, field, output);
        else if(model == "Product") 
            (new ProductController())->runSortingExperimentWith(meth, dbsize, fsize, times, field, output);
        else std::cout << "---> Invalid Model Name" << std::endl;

    } else if (console::exist(argv, argv + argc, "--btree-ins")) { // run experiment with data listed in 
        std::string model = console::get(argv, argv + argc, "--btree-ins");
        std::string field = console::get(argv, argv + argc, "--field");
        std::string size = console::get(argv, argv + argc, "--size");
        std::string times = console::get(argv, argv + argc, "--times");

        if(model == "Customer") 
            (new CustomerController())->btreeIndex(field, size, times);
        else if(model == "Product") 
            (new ProductController())->btreeIndex(field, size, times);
        else std::cout << "---> Invalid Model Name" << std::endl;

    } else if (console::exist(argv, argv + argc, "--queryfixedBtree")) { // query
        // ./a.out --queryfixed CIndex --size 3 --times 1 --out query/query1.o
        std::string strategy = console::get(argv, argv + argc, "--queryfixedBtree");
        std::string psize = console::get(argv, argv + argc, "--psize");
        std::string csize = console::get(argv, argv + argc, "--csize");
        std::string times = console::get(argv, argv + argc, "--times");
        std::string out = console::get(argv, argv + argc, "--out");
        

        (new QueryDatabase())->queryfixedBtree(strategy, out, csize, psize, times);

    } else if (console::exist(argv, argv + argc, "--queryfixedSorted")) { // query

        // ./a.out --queryfixedSorted A --psize 4 --csize 3 --times 1 --out query/query1.o
        std::string strategy = console::get(argv, argv + argc, "--queryfixedSorted");
        std::string psize = console::get(argv, argv + argc, "--psize");
        std::string csize = console::get(argv, argv + argc, "--csize");
        std::string times = console::get(argv, argv + argc, "--times");
        std::string out = console::get(argv, argv + argc, "--out");
        

        (new QueryDatabase())->queryFixedSorted(strategy, out, psize, csize, times);

    } else {
        std::cout << "Is mandatory pass --model (Customer, Product) and --samples (1 , 1000 ....) for run this app";
        std::cout << std::endl;
    }

    // std::cout << "Press enter to continue ...";
    // std::cin.get();

    return 0;
}

int main (int argc, char *argv[]) {
    try
    {
        return handlecommands (argc, argv);
    }
    catch(const std::exception&)  // Consider using a custom exception type for intentional
    {                             // throws. A good idea might be a `return_exception`.
        cerr << endl << "error running the code" << endl;
        return EXIT_FAILURE;
    }
}


