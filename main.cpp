#include <utility>
#include <algorithm>
#include <iostream>
#include <cstdlib>
#include <string>
#include <vector>


#include "src/utils.h"

#include "src/AbstractHeap.h"
#include "src/Heap.h"
#include "src/BinomialTree.h"
#include "src/BinomialHeap.h"
#include "src/FibonacciHeap.h"

#include "src/Dijkstra.h"
#include "src/Experiment.h"
#include "src/Tests.h"

using namespace std;


int handlecommands(int argc, char *argv[]) {
    // define console commands
    std::cout << "INITIALIZING THE EXTERNAL MEMORY EXPERIMENT" << std::endl;
    // if (console::exist(argv, argv + argc, "--runTests") && console::exist(argv, argv + argc, "--samples")) {
    if (console::exist(argv, argv + argc, "--runTests")) {
        (new Tests())->runAllTests();
    }else if (console::exist(argv, argv + argc, "--experiment")) {
        if (!console::exist(argv, argv + argc, "--times") || !console::exist(argv, argv + argc, "--edges")) {
            cout << "missing option --times, --edges" << endl;
            return 0;
        }
            
        std::string times = console::get(argv, argv + argc, "--times");
        std::string edges = console::get(argv, argv + argc, "--edges");
        Experiment *exp = new Experiment();
        int e = stoi(edges);
        int t = stoi(times);
        exp->runExperiment(e, t);
    }else if (console::exist(argv, argv + argc, "--allExperiments")) {
        int t;
        if (console::exist(argv, argv + argc, "--times")){
            std::string times = console::get(argv, argv + argc, "--times");
            t = stoi(times);
        } else {
            t = 10;
        }
         
        (new Experiment())->runAllExperiments(t);
    } else {
        std::cout << "Is mandatory to choose an option (--experiment, --runTests, --allExperiments)";
        std::cout << std::endl;
    }

    // std::cout << "Press enter to continue ...";
    // std::cin.get();

    return 0;
}

int main(int argc, char *argv[]) {
    try
    {
        return handlecommands (argc, argv);
    }
    catch(const std::exception&)  // Consider using a custom exception type for intentional
    {                             // throws. A good idea might be a `return_exception`.
        cerr << endl << "error running the code" << endl;
        return EXIT_FAILURE;
    }
}

void arrayToVect(int **matrix,  vector<vector<int>> graph){
    for(int i = 0; i<8; i++) {
        vector<int> tmp(matrix[i], matrix[i] + sizeof matrix[i] / sizeof matrix[i][0]);
        graph.push_back(tmp);
    }
}

int main2() {
    // tests
    (new Tests())->runAllTests();

    // experiment
    Experiment *exp = new Experiment();
    // exp->runAllExperiments(1);
}
