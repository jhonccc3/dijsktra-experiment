# **Secondary Memory Queries**

This is a task from C4321-2 of Design and Analysis of Algorithms.  



```
> *g++ main.cpp -std=c++11 -o output.out*
> *./output.out*
```

In order to be able of running calculate the cpu running time of a function, add the option `-pthread` to the compiling command

### Sort by field name 
```
> ./output.out --model Product --sortByField rewardPoints

> ./output.out --model Costumer --sortByField id

```
Note: this needs that the folder `tmp` is created

### Run experiment on Sorting

this will run the experiment by taking a database sizes(--dbsize) of 10^7 with 1 replica(--fsize) and run 1 time(--times) for each replica, it will use MS2 as a methodology for sortin (MS2 uses 2 arity for each tree recurtion), on field 'price' (--field).

```
./a.out --run-sorting Product --field price --dbsize 7 --fsize 1 --times 1  --meth MS2 --o exp2out
```
Note: this needs the folder `tmp` to run.

### Run experiment on BTree indexing
create indexe from 10*1 to `--size` (2 in this command) 

```
./a.out --btree-ins Product --field price --size 2 --times 1
```

Note: this needs that the folder `btree` is created

### Run experiment on query Sorting

this will run the experiment for query sorting, on fixed query 

`SELECT * FROM Producto, Consumidor WHERE puntosNec <= puntosAcumulados`

```
$ ./a.out --queryfixedSorted B --psize 4 --csize 3 --times 10 --out query/querySorted.o
```
Note: this needs that the folder `query` is created

### Run experiment on query BTree

this will run the experiment for query, using btree index over fields mentioned
`SELECT * FROM Producto, Consumidor WHERE puntosNec <= puntosAcumulados`
```
./a.out --queryfixedBtree CIndex --csize 4 --psize  3 --times 10 --out query/query1.o
```
Note: this needs that the folder `query` is created

