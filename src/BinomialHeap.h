#include <vector>   
#include <array>

// #include "BinomialTree.h"

using namespace std;

class BinomialHeap {
    public: 
        vector<BinomialTree*> trees;
        static const int CONST_MAX = 1;
        static const int CONST_MIN = 0;
        int priority;

        BinomialHeap() {
            priority = CONST_MIN;
        }

        BinomialHeap(int prior) {
            priority = prior;
        }


        void insert(int val) {
            BinomialTree *b = new BinomialTree(val);
            this->insert(b);
        }

        int heapTop(){
            return (trees[this->getPriorityTree()])->getValue(); 
        }

        int getPriorityTree(){
            BinomialTree *t;
            int j = 0;
            while (trees[j] == NULL)
                j++;

            
            for(int i=j+1; i < trees.size(); i++) {
                if (trees[i] != NULL) {
                    if (priority == CONST_MAX)  {
                        if (trees[j]->getValue() < (trees[i]->getValue())) j = i;
                    } else {
                        if (trees[j]->getValue() > (trees[i]->getValue())) j = i;
                    }
                }
            }

            return j;
        }


        int heapPop(){
            BinomialTree *t;
            int j = this->getPriorityTree();
            t = trees[j];
            trees[j] = NULL;
            this->merge(t->children);
            
            return t->value;
        }

        void merge(vector <BinomialTree*> list) {
            for (int i=0; i < list.size(); i++) {
                if (list[i] != NULL)
                    this->insert(list[i]);
            }
        }

        void print() {
            for(int i=0; i < trees.size(); i++) {
                if (trees[i] != NULL)
                    cout << i << '-' << (trees[i])->getValue() << ',';
            }
            cout << endl;
        }

    private: 
        BinomialTree *insert(BinomialTree *b) {
            
            int ind = (int) log2(b->getSize());
            if (ind >= trees.size()) trees.push_back(NULL);
            // cout << b->getSize() << ',' << ind << endl;

            BinomialTree *o = trees[ind];
            if (o == NULL) {
                trees[ind] = b;
                return b;
            }

            if (b->getValue() > o->getValue()) {
                o = this->unionTreeByPriority(o, b);
            } else {
                o = this->unionTreeByPriority(b, o);
            }

            trees[ind] = NULL;
            this->insert(o);
        }

        BinomialTree *unionTreeByPriority(BinomialTree *min, BinomialTree *max) {
            if (priority == CONST_MAX) {
                max->joinTree(min);
                return max;
            } else {
                min->joinTree(max);
                return min;
            }
        }
};