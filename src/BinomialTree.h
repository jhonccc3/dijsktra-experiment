#include <vector>

using namespace std;

class BinomialTree {
    public: 
        vector <BinomialTree*> children;
        int value, size, key;
        BinomialTree *parent;
        bool marked;

        BinomialTree(int val) {
            value = val;
            size = 1;
            parent = NULL;
            marked = false;
        }

        BinomialTree(int k, int val) {
            value = val;
            size = 1;
            key = k;
            parent = NULL;
            marked = false;
        }

        void joinTree(BinomialTree *t) {
            size += t->getSize();
            children.push_back(t);
            t->setParent(this);
        }

        BinomialTree *findByKey(int k) {
            if (key == k)
                return this;
            if (children.empty())
                return NULL;
            
            BinomialTree *b;
            for(int i=0; i< children.size(); i++) {
                b = children[i]->findByKey(k);
                if(b != NULL) 
                    return b;
            }
            return NULL;
        }

        void removeChild(BinomialTree *b){
            for(int i=0; i< children.size(); i++) {
                if (b == children[i])
                    children.erase(children.begin() + i);
            }
        }

        void mark(){
            marked = true;
        }

        void unmark(){
            marked = false;
        }

        bool isMarked(){
            return marked;
        }

        void setParent(BinomialTree *t) {
            parent = t;
        }

        BinomialTree *getParent() {
            return parent;
        }
        
        int getValue() {
            return value;
        }
        
        void setValue(int val) {
            value = val;
        }

        void setKey(int k) {
            key = k;
        }

        int getSize(){
            return size;
        }

        bool isRoot(){
            return parent ==NULL;
        } 
};