#include <limits>
#include <vector>   
#include <array>
#include <iostream>
#include <cstdlib> 
#include <tuple> 
#include <chrono>
#include <iomanip>
#include <ctime>
#include <thread>


using namespace std;
using namespace chrono;

class Experiment {
    
    public:
        const int N = 100000;
        const int DEFAULT_VAL = 0;

        vector<int> vertices;
        vector<tuple<int,int,int>> connectedEdges;

        Experiment(){
            setUp(N);
        }

        /**
         * Generates data
         **/
        void generateData(int n, int e, 
                                vector<vector<int>> *nodes,
                                vector<tuple<int,int,int>> *edges) {
            // *edges = connectedEdges; // copy connectedEdges
            copy(connectedEdges.begin(), connectedEdges.end(), back_inserter(*edges)); 

            int nEdges = n*e;
            int l = (nEdges-n) + 1;
            int a, b, w;
            
            (*nodes).reserve(n);
            for (int i=0; i < n; i ++ ) {
                vector<int> tmp;
                (*nodes).push_back(tmp);
            }

            // cout << n << "--" << (*nodes).size() << endl;

            for (int i=0; i < l; i ++ ) {
                a = random(n)-1; // include 0 to n-1
                b = random(n)-1;
                if (a != b) {
                    w = randomWeight();
                    (*edges).push_back(make_tuple(a, b, w));
                    (*nodes)[a].push_back((*edges).size()-1);
                    (*nodes)[b].push_back((*edges).size()-1);
                }
                else 
                    i--; //possible bucle
            }
        }
        
        void runExperiment(int e, int times) {
            vector<tuple<int,int,int>> edges;
            vector<vector<int>> nodes;
            cout << "Generating Data " << e << " edges" << endl;
            generateData(N, e, &nodes, &edges);
            cout << "Starting experiment with total edges " << edges.size() << endl;
            int o = random(N);
            vector<int> dist, prev;

            cout << "Array" << '\t';
            for (int i=0; i< times; i++) {
                startTimer();
                Dijkstra::execute(&nodes, &edges, o, &dist, &prev);
                stopTimer();
            }
            cout << endl;
            cout << "NormalHeap" << '\t';
            for (int i=0; i< times; i++) {
                startTimer();
                Dijkstra::executeHeap(&nodes, &edges, o, &dist, &prev, Dijkstra::SIMPLE_HEAP);
                stopTimer();
            }
            cout << endl;
            cout << "FibonacciHeap" << '\t';
            for (int i=0; i< times; i++) {
                startTimer();
                Dijkstra::executeHeap(&nodes, &edges, o, &dist, &prev, Dijkstra::FIBONACCI_HEAP);
                stopTimer();
            }
            cout << endl;
        }

        void runAllExperiments(int times){
            runExperiment(10, times);
            cout << "finished edges 10" << endl;
            runExperiment(100, times);
            cout << "finished edges 100" << endl;
            runExperiment(1000, times);
            cout << "finished edges 1000" << endl;
        }

    private:
        /******* timer coder  *******/
        high_resolution_clock::time_point tini, tend;
        std::clock_t cini, cend;

        void startTimer() {
            tini = high_resolution_clock::now();
            cini = std::clock();
        }

        void stopTimer(){
            cend = std::clock();
            tend = high_resolution_clock::now();
            
            auto duration = std::chrono::duration<double, std::milli>(tend-tini).count();
            auto durationCpu = 1000.0 * (cend - cini) / CLOCKS_PER_SEC;
            
            // (*outlist).push_back(durationCpu);
            // cout << duration << '\t';
            cout << durationCpu << '\t';
        }

        void print(vector<tuple<int,int,int>> *edges) {
            int a,b,c;
            cout << (*edges).size() << endl;
            for (int i = 0; i< (*edges).size() ; i++) {
                std::tie(a,b,c) = (*edges)[0];
            }
        }

        // values from 1 to n.
        int random(int n) {
            // return randomSeed(n, time(0));
            return (rand() % n) + 1;
        }

        /**
         * @deprecated
         *  values from 1 to n with seed.
         **/
        int randomSeed(int n,  int seed) {
            srand(seed);
            return (rand() % n) + 1;
        }
    
        /**
         * values from 1 to 10.
         **/
        int randomWeight() {
            return random(10);
        }

        // create a matrix with all items connected
        void setUp(int n) {
            for (int i=1; i < n; i ++ )
                connectedEdges.push_back(make_tuple(i, i-1, 0));
        }

        void reWeight(vector<tuple<int,int,int>> *edges) {
            for (int i=0; i<(*edges).size(); i++) {
                updateTuple((*edges)[i], randomWeight());
            }
        }

        void updateTuple(tuple<int,int,int> &t, int v) {
            get<2>(t) = v;
        }

};