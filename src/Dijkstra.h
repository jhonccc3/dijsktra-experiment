#include <limits>
#include <vector>   
#include <array>
#include <tuple>

using namespace std;

class Dijkstra {

    public:
        static const int FIBONACCI_HEAP =1, SIMPLE_HEAP =2;

        static void execute(vector<vector<int>> *nodes, 
                                vector<tuple<int,int,int>> *edges, 
                                int origin,
                                vector<int> *dist, 
                                vector<int> *prev) {
            
            // vector<vector<int>> (*graph) = *graph;
            const int infinite = numeric_limits<int>::max();
            // cout << (*nodes).size() << 'x' << (*nodes)[0].size() << endl;

            bool visited[(*nodes).size()];
            
            for (int i=0; i<(*nodes).size(); i++) {
                (*dist).push_back(infinite);
                (*prev).push_back(-1);
                visited[i] = false;
            }
            (*dist)[origin] = 0;
            // visited[origin] = true;
            
            int u;
            
            for (int i=0; i<(*nodes).size()-1; i++) {
                int minDist = infinite;
                int minNodo = -1;
                
                for (int j=0; j<(*nodes).size(); j++) {
                    // cout << (*dist)[j] << "<<"  << minDist<< endl;
                    if ( !visited[j] && (*dist)[j] < minDist ) {
                        minDist = (*dist)[j];
                        minNodo = j;
                    }
                }

                u = minNodo; 
                visited[u]=true;

                vector<int> adjs, weights;
                
                Dijkstra::neightboursOf(u, nodes, edges, &adjs, &weights);
  
                for(int j=0; j < adjs.size(); j++) {
                    if ((*dist)[adjs[j]] > (*dist)[u] + weights[j]) {
                        (*dist)[adjs[j]] = (*dist)[u] + weights[j];
                        (*prev)[adjs[j]] = u;
                    }
                }
            }
        }

        /**
         * saves in @adj the indexes of neightbours of @node.
         * weigts are non negative values
         **/
        static vector<int> *neightboursOf(int node, 
                                            vector<vector<int>> *nodes, 
                                            vector<tuple<int,int,int>> *edges, 
                                            vector<int> *adjs, 
                                            vector<int> *weights) {
            
            vector<int> n = (*nodes)[node];
            int a,b,w;
            for(int j=0; j < n.size(); j++) {
                std::tie(a,b,w) = (*edges)[n[j]];

                if(node == a) {
                    (*adjs).push_back(b);
                } else if (node == b) {
                    (*adjs).push_back(a);
                } else {
    
                    throw exception();
                }
                (*weights).push_back(w);
            }
        }

        /**
         * Heap implementation
         **/
        static void executeHeap(vector<vector<int>> *nodes, 
                                vector<tuple<int,int,int>> *edges, 
                                int origin,
                                vector<int> *dist, 
                                vector<int> *prev, int strategy) {
            const int infinite = numeric_limits<int>::max();
            AbstractHeap *heap = createHeapInstance(strategy);

            for (int i = 0; i < (*nodes).size(); i++) {
                if (i == origin)
                    (*dist).push_back(0);
                else 
                    (*dist).push_back(infinite);

                (*prev).push_back(-1);
                heap->insert(i, (*dist)[i]);
            }
            
            while(!heap->isEmpty()) {
                // heap->print();
                int m = heap->extractMin();
                
                vector<int> adjs, weights;
                neightboursOf(m, nodes, edges, &adjs, &weights);
                
                for (int j = 0; j < adjs.size(); j++) {
                    int v = adjs[j];
                    int newDist = (*dist)[m] + weights[j];
                    if (newDist < (*dist)[v]) {
                        (*dist)[v] = newDist;
                        (*prev)[v] = m;
                        heap->decreaseKey(v, newDist);
                    }
                }
            }
        }

        static AbstractHeap *createHeapInstance(int strategy) {
            if (FIBONACCI_HEAP == strategy)
                return new FibonacciHeap();
            if (SIMPLE_HEAP == strategy)
                return new Heap();
        }
};