#include <limits>
#include <vector>
#include <array>

using namespace std;

//__DEPRECATED
class Node {
    public: 
        int value;
        vector<int> neightbours;

        Node(int value, vector<int> v) {
            this->value = value;
            neightbours = v;
        }
};