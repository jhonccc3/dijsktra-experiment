#ifndef GESTORDB_2_2018_QUERYDBATABASE_H
#define GESTORDB_2_2018_QUERYDBATABASE_H

#endif

#include <vector>
#include <string>
#include <array>

#include <chrono>
#include <iomanip>
#include <ctime>
#include <thread>

using namespace std;
using namespace chrono;

class QueryDatabase : public AbstractController{

private:
    BTree *root;

public: 

    void queryfixedBtree(string strategy, string fname, string csize, string psize, string stimes) {
        int custsize = stoi(csize);
        int prodsize = stoi(psize);
        int times = stoi(stimes);

        for(int i = 1; i <= custsize;i ++) {
            for(int j = 1; j <= prodsize;j ++) {
                // run convinations
                cout << "10^" << "\tc10^" << i << "/p10^" << j << '\t';
                this->queryBtreefixed(strategy, fname, i, j, times);
                cout << endl;
            }
        }
    }
   
    void queryBtreefixed(string strategy, string fname, int csize, int psize, int times)  {
        
        CustomerController *custc = new CustomerController();
        ProductController *prodc = new ProductController();

        int custsize = pow(10, csize);
        int prodsize = pow(10, psize);
        string customerdb = fname + "-cust.db";
        string productdb = fname + "-prod.db";

        custc->generateData(custsize, customerdb);
        prodc->generateData(prodsize, productdb);

        

        //catch fields
        int prodfield = Product::validateAndConvertFieldName("necessaryPoints");
        int custfield = Customer::validateAndConvertFieldName("accumulatePoints");
        // generate Btree indexes
        this->cleanBtreeFolder();
        this->instantianteBTree(prodc, "ptree", Node::PRODUCT_TYPE, prodfield, productdb);
        this->instantianteBTree(custc, "ctree", Node::CUSTOMER_TYPE, custfield, customerdb);
        
        for(int k = 0; k < times;k ++) {
            this->startTimer();
            if (strategy == "CIndex") { //customer index
                this->queryWithCustomerIndex(custc, customerdb, productdb, fname, custfield, prodfield);
            } else if (strategy == "PIndex") {
                this->queryWithProductIndex(prodc, customerdb, productdb, fname, custfield, prodfield);
            } else {
                cout << "error (options are: CIndex, PIndex)"<<endl;
            }
            this->stopTimer();
        }
        
    }

    void queryWithProductIndex(AbstractController *c, string customerdb, string  productdb, string ofilename, int custField, int prodField ) {
        ifstream prodfile, custfile; 
        prodfile.open(productdb.c_str(), ios::in);
        custfile.open(customerdb.c_str(), ios::in);
        
        string touple;
        Node *n;
        
        int i = 0;
        vector <string> out;
        string tmpfname;
        Field* compF;
        while (getline(custfile, touple)) {
            n = Customer::createInstance(touple);

            vector<string> resnames;
            tmpfname = ofilename + to_string(i);
            compF = n->getFieldObject(custField);
            
            c->getBTreeRoot()->queryAllLessThan(compF, &resnames);
            
            joinFiles(touple, &resnames, tmpfname);
            out.push_back(tmpfname);
            i++;
        }
        this->joinFiles(&out, ofilename);
        
    }

    void queryWithCustomerIndex(AbstractController *c, string customerdb, string  productdb, string ofilename, int custfield, int prodfield) {
        ifstream prodfile; 
        prodfile.open(productdb.c_str(), ios::in);
        
        string touple;
        Node *n;

        int i = 0;
        vector <string> out;
        while (getline(prodfile, touple)) {
            n = Product::createInstance(touple);
            vector<string> resnames;

            string tmpfname = ofilename + to_string(i);
            c->getBTreeRoot()->queryAllGreatherThan(n->getFieldObject(prodfield), &resnames);
            
            joinFiles(touple, &resnames, tmpfname);
            out.push_back(tmpfname);
            i++;
        }
        
        this->joinFiles(&out, ofilename);
        prodfile.close();
    }

    void joinFiles(string prepend, vector<string> *resnames, string name) {
        fstream outf (name.c_str(), ios::out);
        string line;
        for(int j =0; j<(*resnames).size(); j++ ) {
            ifstream a((*resnames)[j]);
            // cout << (*resnames)[j] << '\t';
            while(getline(a, line))
                outf << prepend << ',' << line << endl;
            // outf << a.rdbuf();
        }
        // cout << name <<  endl;
        outf.close();
    }

    void joinFiles(vector<string> *resnames, string name) {
        // cout << "finishing with end join"<< endl;
        fstream outf (name.c_str(), ios::out);
        for(int j =0; j<(*resnames).size(); j++ ) {
            ifstream a((*resnames)[j]);
            outf << a.rdbuf();
            remove((*resnames)[j].c_str()); 
        }
        outf.close();
    }

    void instantianteBTree(AbstractController *c, string indexname, int nodetype, int field, string fname){
        string tree = "btree/" + indexname;
        BTreeNode *newRoot = new BTreeNode(tree, nodetype, field);
        c->setBTreeRoot(newRoot);
        c->getBTreeRoot()->setController(c);
    
        // fill tree
        ifstream dbfile (fname.c_str());
        string touple;
        Node *n;
        while (getline(dbfile, touple)) {
            n = c->createInstance(touple);
            c->getBTreeRoot()->insert(NULL, n);
        }
        dbfile.close();
    }


    /************ query Sorting *************/

    void queryFixedSorted(string strategy, string outfilename, string spsize, string scsize, string stimes){
        // no strategy
        int prodsize = stoi(spsize);
        int custsize = stoi(scsize);
        int times = stoi(stimes);
        if (strategy == "A") {
            // cout << "10^" << "\tc" << custsize << "\tp" << prodsize << '\t';
            cout << "10^" << "\tc10^" << custsize << "/p10^" << prodsize << '\t';
            for(int k = 0; k < times;k ++)
                this->querySorted(custsize, prodsize);
            cout << endl;
        } else if(strategy == "B") {
            for(int i = 1; i <= custsize;i ++) {
                for(int j = 1; j <= prodsize;j ++) {
                    // run convinrations
                    cout << "10^" << "\tc10^" << i << "/p10^" << j << '\t';
                    
                    for(int k = 0; k < times;k ++)
                        this->querySorted(i, j);
                    cout << endl;
                }
            }
        } else {
            cout << "options are (A or B )";
            throw exception ();
        }

    }

    void querySorted(int  csize, int psize) {
        int custsize = pow(10,csize);
        int prodsize = pow(10,psize);

        // create conrollers
        CustomerController  *custc = new CustomerController();
        ProductController   *prodc = new ProductController();

        //create database
        string custfname = "query/customer.db", prodfname = "query/product.db"; 
        custc->generateData(custsize, custfname);
        prodc->generateData(prodsize, prodfname);

        // run sorting
        string custfsorted = custfname + ".sorted", prodfsorted = prodfname +".sorted"; 
        int custField = Customer::validateAndConvertFieldName("accumulatePoints"),
            prodField = Product::validateAndConvertFieldName("necessaryPoints");

        custc->sortDB(custfname, custfsorted, custField);
        prodc->sortDB(prodfname, prodfsorted, prodField);

        //run query
        string outputfile = "query/sortedquery.o";
        this->startTimer();
        // run code
        std::thread t(QueryDatabase::executeFixedQuery,
                            this,
                            custfsorted, 
                            prodfsorted,
                            outputfile,
                            custField,
                            prodField);
        t.join();
        
        this->stopTimer();
    }

    static void executeFixedQuery(QueryDatabase *q, string custfile, string prodfile, string outfilename, int cfield, int pfield) {
        q->runFixedQuery(custfile, prodfile, outfilename, cfield, pfield);
    }

    void runFixedQuery(string custfile, string prodfile, string outfilename, int custField, int prodField) {
        // fstream output (outfilename.c_str(), ios::out);
        ifstream  customers(custfile.c_str());
        ifstream  products(custfile.c_str());

        string custline, prodline;
        Node *c, *p = NULL;
        
        // first product
        getline(products,prodline);
        p = Product::createInstance(prodline);

        // loop variants
        bool finished = false;
        int iter = 0;
        vector <string> vprods, outlist;
        while(getline(customers,custline)) {
            c = Customer::createInstance(custline);

            while(!finished && c->isGreatherThan(p->getFieldObject(prodField), custField)) {
                // output << custline << ',' << prodline << endl;
                vprods.push_back(prodline);

                if (getline(products,prodline))
                    p = Product::createInstance(prodline);
                else
                    finished = true;
            }
            iter ++;
            outlist.push_back(outfilename + to_string(iter));
            this->generateSortingList(custline, &vprods, outlist.back());
        }

        this->joinFiles(&outlist, outfilename);

    }

    void generateSortingList(string cust, vector<string> *prods, string outfname) {
        // fstream output (outfname.c_str(), ios::out);
        fstream output (outfname.c_str(), ios::app);
        // cout << "generating li" << (*prods).size() << endl;
        for (int i =0; i< (*prods).size(); i++) {
            output << cust << ',' << (*prods)[i] << endl;
        }
        output.close();
    }


    /******* timer coder  *******/
    high_resolution_clock::time_point tini, tend;
    std::clock_t cini, cend;

    void startTimer() {
        tini = high_resolution_clock::now();
        cini = std::clock();
    }

    void stopTimer(){

        cend = std::clock();
        tend = high_resolution_clock::now();
        
        auto duration = std::chrono::duration<double, std::milli>(tend-tini).count();
        auto durationCpu = 1000.0 * (cend - cini) / CLOCKS_PER_SEC;
        
        // (*outlist).push_back(durationCpu);
        // cout << duration << '\t';
        cout << durationCpu << '\t';
    }

};
