#include <tuple> 
#include <limits>
#include <vector>   
#include <array>

using namespace std;

class Tests {

    public:

        void runAllTests(){
            runTestsBinomialHeap();
            runTestsFibHeap();
            runTestsDijkstra();
            runTestsHeap();
            runTestsExp();
        }

        bool assert(int value, int expected){
            bool res = value != expected;
            if (res)
                cout << "test did not pass: given: " << value << " expected: "  << expected << endl;
            
            return res;
        }

        bool assert(bool flag){
            if (!flag)
                cout << "Error test: given: false expected: "  << "true" << endl;

            return flag;
        }

        void runTestsBinomialHeap() {
            BinomialHeap *bq = new BinomialHeap();
            for(int i=0; i<100; i++) {
                bq->insert(i);
            }
            assert(bq->heapTop(), 0);
            assert(bq->heapPop(), 0);
            assert(bq->heapTop(), 1);
            assert(bq->heapPop(), 1);
            assert(bq->heapTop(), 2);
            assert(bq->heapPop(), 2);

            bq = new BinomialHeap(BinomialHeap::CONST_MAX);
            for(int i=0; i<100; i++) {
                bq->insert(i);
            }
            assert(bq->heapTop(), 99);
            assert(bq->heapPop(), 99);
            assert(bq->heapTop(), 98);
            assert(bq->heapPop(), 98);
            assert(bq->heapTop(), 97);
            assert(bq->heapPop(), 97);
            cout << "Finished Tests on binomial Heap Class" << endl;
        }

        void runTestsHeap() {
            Heap *bq = new Heap();
            for(int i=0; i<100; i++) {
                bq->insert(i,i);
            }
            
            assert(bq->extractMin(), 0);
            assert(bq->extractMin(), 1);
            assert(bq->extractMin(), 2);

            // empty tests
            bq = new Heap();
            for(int i=0; i<2; i++) {
                bq->insert(i, i);
            }
            assert(!bq->isEmpty());
            assert(bq->extractMin(), 0);
            assert(bq->extractMin(), 1);
            assert(bq->isEmpty());

            bq->insert(3,3);
            assert(bq->getMin(),3);
            assert(!bq->isEmpty());
            assert(bq->extractMin(),3);
            assert(bq->isEmpty());

            // //decreaseKey 
            for(int i=5; i<20; i++) {
                bq->insert(i, i);
            }

            bq->decreaseKey(19, 4);
            assert(bq->getMin(),19);
            bq->decreaseKey(15, 3);
            assert(bq->getMin(), 15);
            bq->decreaseKey(9, 0);
            assert(bq->getMin(), 9);

            cout << "Finished Tests on Heap Class" << endl;
        }

        void runTestsFibHeap() {
            FibonacciHeap *fq = new FibonacciHeap();
            for(int i=0; i<100; i++) {
                fq->insert(i, i);
            }
            assert(fq->heapTop(), 0);
            assert(fq->heapPop(), 0);
            assert(fq->heapTop(), 1);
            assert(fq->heapPop(), 1);
            assert(fq->heapTop(), 2);
            assert(fq->heapPop(), 2);

            fq = new FibonacciHeap(BinomialHeap::CONST_MAX);
            for(int i=0; i<100; i++) {
                fq->insert(i, i);
            }
            assert(fq->heapTop(), 99);
            assert(fq->heapPop(), 99);
            assert(fq->heapTop(), 98);
            assert(fq->heapPop(), 98);
            assert(fq->heapPop(), 97);
            assert(fq->heapTop(), 96);


            // empty tests
            fq = new FibonacciHeap();
            for(int i=0; i<2; i++) {
                fq->insert(i, i);
            }
            assert(!fq->isEmpty());
            assert(fq->heapPop(), 0);
            assert(fq->heapPop(), 1);
            assert(fq->isEmpty());

            fq->insert(3,3);
            assert(fq->heapTop(),3);
            assert(!fq->isEmpty());
            assert(fq->heapPop(),3);
            assert(fq->isEmpty());

            // //decreaseKey 
            for(int i=5; i<20; i++) {
                fq->insert(i, i);
            }
            fq->heapPop();
            fq->decreaseKey(19, 4);
            assert(fq->heapTop(),19);
            fq->decreaseKey(15, 3);
            assert(fq->heapTop(), 15);
            fq->decreaseKey(9, 0);
            assert(fq->heapTop(), 9);

            cout << "Finished Tests on Fibonacci Heap Class" << endl;
        }

        void matrixToEdges(vector<vector<int>> *matrix, 
                            vector<tuple<int,int,int>> *edgs, 
                            vector<vector<int>> *nodes){

            (*nodes).reserve((*matrix).size());
            for(int i = 0; i<(*matrix).size(); i++) {
                vector<int>tmp;
                (*nodes).push_back(tmp);
            }

            for(int i = 0; i<(*matrix).size(); i++) {
                for(int j=i+1; j<(*matrix).size(); j++) {
                    if ((*matrix)[i][j] >= 0) {
                        (*edgs).push_back(make_tuple(i,j, (*matrix)[i][j]));
                        (*nodes)[i].push_back((*edgs).size()-1);
                        (*nodes)[j].push_back((*edgs).size()-1);
                    }
                }
            }
        }

        void runTestsDijkstra() {
            int x[8][8] = { {-1,-1,8,9,4,-1,-1,-1}, 
                            {-1,-1,-1,-1,-1,-1,7,-1}, 
                            {8,-1,-1,-1,-1,-1,8,-1}, 
                            {9,-1,-1,-1,-1,-1,-1,-1}, 
                            {4,-1,-1,-1,-1,-1,-1,2}, 
                            {-1,-1,-1,-1,-1,-1,-1,7}, 
                            {-1,7,8,-1,-1,-1,-1,-1}, 
                            {-1,-1,-1,-1,2,7,-1,-1}};

            std::vector<vector<int>> graph;
            for(int i = 0; i<8; i++) {
                vector<int> tmp(x[i], x[i] + sizeof x[i] / sizeof x[i][0]);
                graph.push_back(tmp);
            }

            vector<int> cost, prev;
            int ps[8] = {2,-1,6,0,0,7,1,4};
            int cs[8] = {23,0,15,32,27,36,7,29};
            // int* a = &v[0];
            vector<int> prevExp(ps, ps + sizeof ps / sizeof cs[0]);
            vector<int> costExp(cs, cs + sizeof cs / sizeof cs[0]);

            vector<vector<int>> nodes; 
            vector<tuple<int,int,int>> edges; 

            matrixToEdges(&graph, &edges, &nodes);

  
            Dijkstra::execute(&nodes, &edges, 1, &cost, &prev);
            assertArray(&cost, &costExp);
            assertArray(&prev, &prevExp);

            // heap dijkstra simple heaps
            cost.clear();
            prev.clear();
            Dijkstra::executeHeap(&nodes, &edges, 1, &cost, &prev, Dijkstra::FIBONACCI_HEAP);
            assertArray(&cost, &costExp);
            assertArray(&prev, &prevExp);

            // heap dijkstra fibonacci heaps
            cost.clear();
            prev.clear();
            Dijkstra::executeHeap(&nodes, &edges, 1, &cost, &prev, Dijkstra::SIMPLE_HEAP);
            assertArray(&cost, &costExp);
            assertArray(&prev, &prevExp);
            cout << "Finished Tests on Dijkstra Class" << endl;
        }


        bool assertArray(vector<int> *vs, vector<int> *es){
            if ((*vs).size() != (*es).size()) {
                cout << "Test array error: size did not match " 
                            << (*vs).size() <<":" << (*es).size()  << endl;
                return false;
        
            }
                
            for (int i=0; i< (*es).size(); i++) {
                assert((*vs)[i], (*es)[i]);
            }
        }

        void runTestsExp() {
            Experiment *e = new Experiment();

            vector<vector<int>> nodes;
            vector<tuple<int,int,int>> edges;
            e->generateData(1000, 100, &nodes, & edges);

            int a,b,w;
            for(int i=0; i<nodes.size();i++) {
                for(int j=0; j<nodes[i].size();j++) {
                    tie(a,b,w) = edges[nodes[i][j]];
                    if(a != i && b != i) 
                        "error test: on generate Data Exp";
                }
            }
            
            // e->runExperiment(10, 1);
            cout << "Finished Tests on Experiment Class" << endl;
        }

};