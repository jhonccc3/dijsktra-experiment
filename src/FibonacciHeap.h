#include <vector>   
#include <array>

// #include "BinomialTree.h"

using namespace std;

class FibonacciHeap : public AbstractHeap {
    public: 
        vector<BinomialTree*> trees;
        static const int CONST_MAX = 1;
        static const int CONST_MIN = 0;
        int priority, heapNodeSize = 0;
        

        bool isValueSet = false;
        BinomialTree *priorTree;

        FibonacciHeap() {
            priority = CONST_MIN;
        }

        FibonacciHeap(int prior) {
            priority = prior;
        }

        // deprecated
        // void insert(int val) {
        //     BinomialTree *b = new BinomialTree(val);
        //     insert(b, val);
        // }

        void insert(int key, int val) {
            BinomialTree *b = new BinomialTree(key, val);
            insert(b, val);
        }

        void insert (BinomialTree *b, int val) {
            trees.push_back(b);
            setPriorityValue(b);
            heapNodeSize ++;
        }

        bool isEmpty() {
            return trees.empty();
        }

        int extractMin() {
            return heapPop();
        }

        int decreaseKey(int k, int val) {
            BinomialTree* b;
            for (int i= 0;i< trees.size(); i++) {
                b = trees[i]->findByKey(k);
                if (b != NULL) {
                    b->setValue(val);
                    b->mark();
                    if (!b->isRoot() && b->value < b->getParent()->value) {
                        this->cutTree(b);
                    }
                    setPriorityValue(b);
                    return b->key;
                }
            }
            return -1;
        }

        void cutTree(BinomialTree *b) {
            if(!b->isMarked() || b->isRoot())
                return;
            
            b->unmark();
            trees.push_back(b);
            BinomialTree *p = b->getParent();
            b->setParent(NULL);
            p->removeChild(b);

            if (p->isMarked())
                cutTree(p);
            else
                p->mark();
        }

        int findTreeByKey(int key) {
            for (int i=0; i < trees.size(); i++) {
                if (trees[i]->key == key)
                    return i;
            }
            return -1;
        }

        int heapTop(){
            return priorTree->key; 
        }

        int heapPop(){
            BinomialTree *t;
            int j = this->getPriorityTree();
            t = trees[j];
            trees.erase(trees.begin() + j);
            
            this->merge(t->children);

            this->heapify(&trees);
            heapNodeSize --;
            return t->key;
        }

        void merge(vector <BinomialTree*> list) {
            for (int i=0; i < list.size(); i++) {
                trees.push_back(list[i]);
            }
        }

        void heapify(vector <BinomialTree*> *list) {
            int size = (int) log2(heapNodeSize);
            vector <BinomialTree*> newTrees(size, NULL);
            
            isValueSet = false;
            
            for (int i=0; i < (*list).size(); i++) {
                if ((*list)[i] != NULL) {
                    this->insertInHeap(&newTrees, (*list)[i]);
                    this->setPriorityValue((*list)[i]);
                }
            }
            
            trees = newTrees;

            removeNullTrees();
            // this->print();
        }

        void print() {
            for(int i=0; i < trees.size(); i++) {
                if (trees[i] != NULL)
                    cout << (trees[i])->key << '-' << (trees[i])->getValue() << ',';
            }
            cout << endl;
        }

    private: 

        void removeNullTrees() {
            for (int i=0; i < trees.size(); i++) {
                if(trees[i] == NULL)
                    trees.erase(trees.begin() + i);
            }
        }

        BinomialTree *insertInHeap(vector<BinomialTree*> *list, BinomialTree *b) {
            int ind = (int) log2(b->getSize());
            if (ind >= (*list).size()) (*list).push_back(NULL);
            
            BinomialTree *o = (*list)[ind];
            if (o == NULL) {
                (*list)[ind] = b;
                return b;
            }

            if (b->getValue() > o->getValue()) {
                o = this->unionTreeByPriority(o, b);
            } else {
                o = this->unionTreeByPriority(b, o);
            }

            (*list)[ind] = NULL;
            this->insertInHeap(list, o);
        }

        int getPriorityTree(){
            BinomialTree *t;
            int j = 0;
            while (trees[j] == NULL)
                j++;

            
            for(int i=j+1; i < trees.size(); i++) {
                if (trees[i] != NULL) {
                    if (priority == CONST_MAX)  {
                        if (trees[j]->getValue() < (trees[i]->getValue())) j = i;
                    } else {
                        if (trees[j]->getValue() > (trees[i]->getValue())) j = i;
                    }
                }
            }

            return j;
        }

        BinomialTree *unionTreeByPriority(BinomialTree *min, BinomialTree *max) {
            if (priority == CONST_MAX) {
                max->joinTree(min);
                return max;
            } else {
                min->joinTree(max);
                return min;
            }
        }

        void setPriorityValue(BinomialTree *val) {
            if (!isValueSet) {
                priorTree = val;
                isValueSet = true;
            } else {
                if (priority == CONST_MAX) {
                    if (priorTree->value < val->value)  priorTree = val;
                } else {
                    if (priorTree->value > val->value)  priorTree = val;
                }
            }
        }
};