#include <vector>   
#include <array>

// #include "BinomialTree.h"

using namespace std;


class AbstractHeap {
    
public:
    virtual void insert(int k, int v){}
    virtual bool isEmpty(){}
    virtual int extractMin(){}
    virtual int decreaseKey(int k, int v){}
    
};
