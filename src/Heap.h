#include <vector>   
#include <array>

// #include "BinomialTree.h"

using namespace std;


class Node {
    public:
        int key, value;
        Node(int k, int v) {
            key = k;
            value = v;
        }
};

class Heap : public AbstractHeap {
    public: 
        vector<Node*> heap;

        void insert(int k, int v) {
            Node *node = new Node(k, v);
            insert(node);
        }

        void insert(Node *n)  {
            heap.push_back(n);
            bubbleUp(heap.size()-1);
        }

        bool isEmpty() {
            return heap.empty();
        }

        int getMin() {
            return heap.front()->key;
        }

        int extractMin() {
            Node *res, *n;
            res = heap.front();
            n = heap.back();
            heap.erase(heap.begin() + heap.size()-1);
            heap[0] = n;
            bubbleDown(0);
            return res->key;
        }

        void bubbleDown(int ind) {
            int childInd = (ind *2) +1;
            if (childInd >= heap.size())
                return;

            int minInd;
            if (childInd < heap.size() && heap[childInd+1]->value < heap[childInd]->value) {
                minInd = childInd +1;
            } else {
                minInd = childInd;
            }

            if (heap[ind]->value > heap[minInd]->value) {
                Node* tmp = heap[ind];
                heap[ind] = heap [minInd];
                heap[minInd] = tmp;
                bubbleDown(minInd);
            }
        }

        void bubbleUp(int ind) {
            if (ind == 0)
                return;
            int parentInd = (ind -1) /2;
            if (heap[ind]->value < heap[parentInd]->value) {
                Node* tmp = heap[ind];
                heap[ind] = heap [parentInd];
                heap[parentInd] = tmp;
                bubbleUp(parentInd);
            }
        }

        int decreaseKey(int k, int v){
            int ind = findKey(k);
            Node *n = heap[ind];
            n->value = v;

            bubbleUp(ind);
            return ind;
        }

        int findKey(int k) {
            for (int i=0; i< heap.size(); i++) {
                if(heap[i]->key == k) {
                    return i;
                }
            }
        }

};
